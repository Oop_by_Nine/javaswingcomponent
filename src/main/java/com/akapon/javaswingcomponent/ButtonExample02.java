/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.javaswingcomponent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;


/**
 *
 * @author Nine
 */
public class ButtonExample02 {

    ButtonExample02() {
        JFrame f = new JFrame("Button Example02");
        JButton b = new JButton(new ImageIcon("D:\\icon0.png"));
        b.setBounds(100, 100, 100, 150);
        f.add(b);
        f.setSize(300, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ButtonExample02();
    }
}

